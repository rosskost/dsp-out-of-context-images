### Importing releveant libraries

# Basic
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

# Neural Net
from keras.models import Sequential
from keras.layers import Dense, Conv1D, Embedding, Activation, LSTM, Dropout
from keras.preprocessing.sequence import pad_sequences

# NLP
from  keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence

from sklearn.feature_extraction.text import TfidfVectorizer


# Visualization
import matplotlib.pyplot as plt

# load and assign to objects
fake = pd.read_csv("Fake.csv")
real = pd.read_csv("True.csv")

# label data sets
fake['fake'] = 1
real['fake'] = 0

# check 
#print(fake.head(2))
#print(real.head(2))

# combine both datasets
combined = pd.concat([fake, real])
combined

# Train test split
features = combined["title"]
labels = combined["fake"]

X_train, X_test, y_train, y_test = train_test_split(
    features, labels,
    random_state = 42
)

# Hyperparameter
max_words = 2000
max_len = 400

# Create tokanizer
token = Tokenizer(num_words = max_words, lower = True, split = ' ')

# Tokanizer Words
token.fit_on_texts(X_train.values)
sequences = token.texts_to_sequences(X_train.values)

train_sequence_padded = pad_sequences(sequences, maxlen = max_len)
train_sequence_padded.shape

embed_dim = 50
lstm_out = 64

model = Sequential()
model.add(Embedding(max_words, embed_dim, input_length = max_len))
model.add(Conv1D(64, 5, activation = 'relu'))
model.add(LSTM(lstm_out))
model.add(Dense(256))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1, name = 'out_layer'))
model.add(Activation('sigmoid'))

model.compile(loss ='binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
print(model.summary())

model.fit(train_sequence_padded,
    y_train,
    epochs=4,
    batch_size=1000)

test_sequences = token.texts_to_sequences(X_test)
test_sequence_padded = pad_sequences(test_sequences, maxlen = max_len)

model.evaluate(test_sequence_padded, y_test)