import os
from flask import Flask, render_template, url_for, request, redirect
import numpy as np
import pandas as pd
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
from keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16
import joblib # load previously fitted PCA model
from random import randint
# Distance measure to find closest image in feature representation
from scipy.spatial import distance
# Import OOC-evaluation and COSMOS model:
from out_of_context_model.full_pipeline import get_prediction_website_input
from out_of_context_model.utils_inference.config import image_threshold
# Fake News and Hate Speech Scores
from additional_models.generate_plots_addons import *

# Directories
cwd_path = os.getcwd()
utils_matching_img_path = os.path.join(cwd_path, "utils_matching_image")
upload_folder = "static/uploads"

vgg = VGG16(weights=os.path.join(cwd_path,
	"saved_weights",
	"vgg16_weights_tf_dim_ordering_tf_kernels.h5"),
	 include_top=True)

# load previously fitted model
pca = joblib.load(os.path.join(utils_matching_img_path, 'pca_fit_features.pkl').replace("\\","/"))

# load feature data frame
df_feat_load = pd.read_csv(os.path.join(utils_matching_img_path, "look_up_pca.csv").replace("\\","/"), sep = ',')

# load captions df
df_captions = pd.read_json(os.path.join(utils_matching_img_path, "test_data.json").replace("\\","/"), lines = True)

# helper function to load and prepocess images
def load_image(p):
    img = image.load_img(p, target_size=vgg.input_shape[1:3])
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return img, x

# Extract last fully connected layer in model to get image embedding
feat_extractor = Model(inputs=vgg.input, outputs=vgg.get_layer("fc2").output)

# No imports below this point

##########################
### Webapp starts here ###
##########################

# App configuration
app = Flask(__name__)
app.config['upload_folder'] = upload_folder

@app.errorhandler(500)
def internal_server_error(e):
    # note that we set the 500 status explicitly
    return render_template('500.html'), 500

@app.route('/')
def home():
	return render_template('home.html')

@app.route('/display/<filename>')
def display_image(filename):
	return redirect(url_for('static', filename = 'uploads/' + filename), code = 301)

@app.route('/results', methods = ['POST', 'GET'])
def results():
	if request.method == 'GET':
		return f"The URL '/results' is accessed directly. Try going to '/' to submit data"
	if request.method == 'POST':
		file = request.files['file']

		# Save image
		filename = file.filename
		filename_with_boxes = 'with_bboxes_' + filename
		file.save(os.path.join(cwd_path, app.config['upload_folder'], filename))
		input_img_path = os.path.join(cwd_path, 'static', 'uploads', filename).replace("\\","/")
		_ , input_img_df = load_image(input_img_path)

		# Get Fake News Score for Caption
		form_data = request.form
		caption = request.form['Caption']

		# Find image closest to upload
		input_features = feat_extractor.predict(input_img_df)
		input_pca_features = pca.transform(input_features)[0]
		distances = [distance.cosine(input_pca_features, feat) for feat in df_feat_load.iloc[:,1:].values]
		closest = np.min(distances)
		img_closest = df_feat_load.names[np.argmin(distances)]

		# Find captions of closest image
		caps = df_captions.loc[df_captions["img_local_path"] == os.path.join('test', img_closest).replace("\\","/"), ['caption1', 'caption2']]
		suggested_caption_1 = str(caps.caption1.values)[2:-2:1]
		suggested_caption_2 = str(caps.caption2.values)[2:-2:1]
		# If suggested captions are already out of context, forward to results_nomatch.html
		suggested_ooc = df_captions.loc[df_captions["img_local_path"] == os.path.join('test', img_closest).replace("\\","/"), ['context_label']]
		suggested_ooc = int(suggested_ooc.context_label.values)

		if suggested_ooc == 1 or caption == "Test" or closest > image_threshold:
			return render_template('results_nomatch.html',
				filename = filename,
				uploaded_caption = caption,
				form_data = form_data)	
		else:
			# Save charts
				# Add random 4 digit string to prevent browser from using cached image
			randy = str(randint(1000, 9999))
			save_fake_news_spdr(caption, suggested_caption_1, suggested_caption_2, os.path.join(cwd_path, app.config['upload_folder'], "fake_spdr_3" + randy + ".png"))
			save_hate_spdr(caption, suggested_caption_1, suggested_caption_2, os.path.join(cwd_path, app.config['upload_folder'], "hate_spdr_3" + randy + ".png"))
			save_fake_bar_chart(caption, suggested_caption_1, suggested_caption_2, os.path.join(cwd_path, app.config['upload_folder'], "fake_bar" + randy + ".png"))
			save_hate_bar_chart(caption, suggested_caption_1, suggested_caption_2, os.path.join(cwd_path, app.config['upload_folder'], "hate_bar" + randy + ".png"))
			save_media_bias(caption, suggested_caption_1, suggested_caption_2, os.path.join(cwd_path, app.config['upload_folder'], "media_bias" + randy + ".png"))

			# Are the two suggested captions Out of Context with uploaded caption?
			OOC_1, sim_score_1 = get_prediction_website_input(input_img_path, caption, suggested_caption_1, drawing_mode = 1)
			OOC_2, sim_score_2 = get_prediction_website_input(input_img_path, caption, suggested_caption_2, drawing_mode = 2)
			sim_score_1 = round(sim_score_1, 3)
			sim_score_2 = round(sim_score_2, 3)
			# Jinja needs boolean for conditional background colors
			is_OOC_1 = bool
			is_OOC_2 = bool
			if OOC_1 == 1:
				OOC_1 = "Your caption and Caption 1 are out of context."
				is_OOC_1 = True
			else:
				OOC_1 = "Your caption and Caption 1 are in context."
				is_OOC_1 = False
			if OOC_2 == 1:
				OOC_2 = "Your caption and Caption 2 are out of context."
				is_OOC_2 = True
			else:
				OOC_2 = "Your caption and Caption 2 are in context."
				is_OOC_2 = False

			# Move image with bounding boxes to uploads folder
			os.replace("test_images_with_boxes/" + filename_with_boxes, "static/uploads/" + filename_with_boxes)
			# Add random 4 digit string to prevent browser from using cached image
			os.replace("static/uploads/" + filename_with_boxes, "static/uploads/" + randy + filename_with_boxes)

			return render_template('results_match.html',
				filename = filename,
				filename_with_boxes = randy + filename_with_boxes,
				uploaded_caption = caption,
				suggested_caption_1 = suggested_caption_1,
				suggested_caption_2 = suggested_caption_2,
				OOC_1 = OOC_1,
				OOC_2 = OOC_2,
				is_OOC_1 = is_OOC_1,
				is_OOC_2 = is_OOC_2,
				sim_score_1 = sim_score_1,
				sim_score_2 = sim_score_2,
				fake_spdr_3 = "fake_spdr_3" + randy + ".png",
				hate_spdr_3 = "hate_spdr_3" + randy + ".png",
				fake_bar = "fake_bar" + randy + ".png",
				hate_bar = "hate_bar" + randy + ".png",
				media_bias = "media_bias" + randy + ".png",
				form_data = form_data)

@app.route('/results_addition', methods = ['POST', 'GET'])
def results_addition():
	if request.method == 'GET':
		return f"The URL '/results_addition' is accessed directly. Try going to '/' to submit data"
	if request.method == 'POST':
		file = request.files['file']

		# Save image
		filename = file.filename
		filename_with_boxes = 'with_bboxes_' + filename
		file.save(os.path.join(app.config['upload_folder'], filename))
		input_img_path = os.path.join(cwd_path, 'static', 'uploads', filename).replace("\\","/")
		_ , input_img_df = load_image(input_img_path)

		# Get Fake News Score for both Caption
		form_data = request.form
		caption = request.form['Caption']
		additional_caption = request.form['Additional_Caption']

		# Save spider charts
		# Add random 4 digit string to prevent browser from using cached image
		randy = str(randint(1000, 9999))
		save_fake_news_spdr(caption, additional_caption, "none", os.path.join(cwd_path, app.config['upload_folder'], "fake_spdr_2" + randy + ".png"))
		save_hate_spdr(caption, additional_caption, "none", os.path.join(cwd_path, app.config['upload_folder'], "hate_spdr_2" + randy + ".png"))
		save_fake_bar_chart(caption, additional_caption, "none", os.path.join(cwd_path, app.config['upload_folder'], "fake_bar_2" + randy + ".png"))
		save_hate_bar_chart(caption, additional_caption, "none", os.path.join(cwd_path, app.config['upload_folder'], "hate_bar_2" + randy + ".png"))
		save_media_bias(caption, additional_caption, "none", os.path.join(cwd_path, app.config['upload_folder'], "media_bias_2" + randy + ".png"))

		# Are the two suggested captions Out of Context with uploaded caption?

		OOC_1, sim_score_3 = get_prediction_website_input(input_img_path, caption, additional_caption, drawing_mode=0)
		sim_score_3 = round(sim_score_3, 3)
		# Jinja needs boolean for conditional background colors
		is_OOC_1 = bool
		if OOC_1 == 1:
			OOC_1 = "Your two captions are out of context."
			is_OOC_1 = True
		else:
			OOC_1 = "Your two captions are in context."
			is_OOC_1 = False

		# Move image with bounding boxes to uploads folder
		os.replace("test_images_with_boxes/" + filename_with_boxes, "static/uploads/" + filename_with_boxes)
		# Add random 4 digit string to prevent browser from using cached image
		os.replace("static/uploads/" + filename_with_boxes, "static/uploads/" + randy + filename_with_boxes)

		return render_template('results_match_addition.html',
			filename = filename,
			filename_with_boxes = randy + filename_with_boxes,
			uploaded_caption = caption,
			additional_caption = additional_caption,
			OOC_1 = OOC_1,
			is_OOC_1 = is_OOC_1,
			sim_score_3 = sim_score_3,
			fake_spdr_2 = "fake_spdr_2" + randy + ".png",
			hate_spdr_2 = "hate_spdr_2" + randy + ".png",
			fake_bar_2 = "fake_bar_2" + randy + ".png",
			hate_bar_2 = "hate_bar_2" + randy + ".png",
			media_bias_2 = "media_bias_2" + randy + ".png",
			form_data = form_data)

@app.route('/about')
def about():
	return render_template('about.html')

if __name__ == "__main__":
	port = int(os.environ.get("PORT", 5000))
	app.run(host='0.0.0.0', port=port)
