# Basic
import os
import numpy as np
import torch

# Models
import torch.nn as nn
from transformers import BertModel, BertTokenizer
from out_of_context_model.utils_inference.config import device

# I downloaded bert and put it here (this will save us boot time)
path_bert_model = os.path.join(os.getcwd(), "saved_weights", "bert_base_uncased")
path_fakenews_model = os.path.join(os.getcwd(), "utils_matching_image", "best_model.bin")


# Seeds
RANDOM_SEED = 42
np.random.seed(RANDOM_SEED)
torch.manual_seed(RANDOM_SEED)

class FakeNewsClassifier(nn.Module):

  def __init__(self, n_classes):
    super(FakeNewsClassifier, self).__init__()
    self.bert = BertModel.from_pretrained(path_bert_model, return_dict=False)
    self.drop = nn.Dropout(p=0.3)
    self.out = nn.Linear(self.bert.config.hidden_size, n_classes)
    self.softmax = nn.Softmax(dim = 1)

  def forward(self, input_ids, attention_mask):
    _, pooled_output = self.bert(
      input_ids=input_ids,
      attention_mask=attention_mask
    )
    output = self.drop(pooled_output)
    output = self.out(output)
    return self.softmax(output)

class_names = ['real', 'fake']

model = FakeNewsClassifier(len(class_names))
model.load_state_dict(torch.load(path_fakenews_model, map_location = device))
model = model.to(device)

def get_fake_news_score(input_title):
	tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
	MAX_LEN = 40

	encoded_title = tokenizer.encode_plus(
	  input_title,
	  max_length=MAX_LEN,
	  add_special_tokens=True,
	  return_token_type_ids=False,
	  pad_to_max_length=True,
	  return_attention_mask=True,
	  return_tensors='pt',
	)

	input_ids = encoded_title['input_ids'].to(device)
	attention_mask = encoded_title['attention_mask'].to(device)
	output = model(input_ids, attention_mask)

	fake_news_score = output[0,1].item()

	return fake_news_score
