# Set data path
path = 'C:/Users/Cornelius/Documents/PY/Flask/Flask_Dev'

# set tensor flow version -> access VGG16 pre-trained Convolutional Neural Net(for picture classification)
#%tensorflow_version 1.x

# General
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

# Image loading/ processing
import cv2
from PIL import Image

# Neural Net
import keras
from keras.preprocessing import image
from keras.applications.imagenet_utils import decode_predictions, preprocess_input
from keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16

# PCA
from sklearn.decomposition import PCA
import joblib # load previously fitted PCA model

# Distance measure to find closest image in feature representation
from scipy.spatial import distance

# Load model for image classification 
#model = keras.applications.VGG16(weights='imagenet', include_top=True)
model = VGG16(weights='imagenet', include_top=True)

# load previously fitted model
pca = joblib.load(os.path.join(path, 'pca_fit_features.pkl').replace("\\","/"))

# load feature data frame
df_feat_load = pd.read_csv(os.path.join(path, "look_up_pca.csv").replace("\\","/"), sep = ',')

# load captions df
df_captions = pd.read_json(os.path.join(path, "test_data.json").replace("\\","/"), lines = True)

# function to load and prepocess images
def load_image(p):
    img = image.load_img(p, target_size=model.input_shape[1:3])
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return img, x

# Extract last fully connected layer in model to get image embedding
feat_extractor = Model(inputs=model.input, outputs=model.get_layer("fc2").output)

# load
input_img_path = os.path.join(path, 'test', '34.jpg').replace("\\","/")
#input_img_path = os.path.join(path, 'img_manipul', 'mani4.jpg')

input_img = cv2.imread(input_img_path)
plt.imshow(input_img[:,:,::-1])

# preprocess
_ , input_img_df = load_image(input_img_path)

# check shape
print(input_img_df.shape)

# Neural Net Embedding
input_features = feat_extractor.predict(input_img_df)

# PCA
input_pca_features = pca.transform(input_features)[0]

# compute cosin distances 
distances = [distance.cosine(input_pca_features, feat) for feat in df_feat_load.iloc[:,1:].values]

# find closest image
img_closest = df_feat_load.names[np.argmin(distances)]

Image.open(os.path.join(path, "test", img_closest).replace("\\","/"))

caps = df_captions.loc[df_captions["img_local_path"] == os.path.join('test', img_closest).replace("\\","/"), ['caption1', 'caption2']]

# format captions
caption1 = str(caps.caption1.values)[2:-2:1]
caption2 = str(caps.caption2.values)[2:-2:1]
print(caption1)
print(caption2)

