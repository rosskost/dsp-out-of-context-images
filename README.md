Out of context Images
===============


This is the repository for the topic "Out-of-context images" of DS500 Data Science Project.

Our Flask App can be rached here: <https://ooc-images.ngrok.io/>

--------------------------------------------------------

This topic builds on the paper [MMSys'21 Grand Challenge on Detecting Cheapfakes](https://arxiv.org/abs/2107.05297).

---------------------------------------------------
## Fex Explanations:

We provide some data:

In data\images_database are all ~ 800 of the test-examples ,that are verified to be in-context. We use these pictures to find a match with
the pictures, that are uploaded by the user, and then link them to the verified captions in the data\annotations folder.

In examples_ooc_images are five random examples of out-of-context examples from the test set.


In the 'additional_code' folder, you find code, that is not used to run the app, but rather code
we spend a lot of time on during developement of this app.

In 'scripts_fine_tuning_models' are scripts, that we ran in GoogleColab and in which we finetuned
the models ,that are used for the different additional scores we introduce.

In 'out_of_context_model' we show how we can do the full pre-processing and use the Baseline Out-of-context model from the paper above,
to predict out-of-context use for any image and caption pair.

We used this method for most of the time during development, before we switched to CLIP and introduced a completly new approach to this problem.

---------------------------------
## Running the app locally:

[*Uploaded to dockerhub after deadline*]

With the Dockerfile in this repo, I created a Docker Image. This way it will be very easy to run the app locally.

Also you don't have to download our weights from file-providers like Dropbox and fiddle things together.

With docker installed on your machine, simply type

```bash
$ docker pull rosskost/flask_app_ooc:latest
```
After pulling the full image , you can run the container via

```bash
$ docker run -p 5000:5000 -e PORT=5000 rosskost/flask_app_ooc:latest
```

and you will have access to the app in your browser via localhost:5000. \
Be aware that this container is computationally expensive an it needs to download a few SentenceTransformers models, that we use as a base for our fine-tuned models.

 -------------------------------------------------------------

Contact
-------

If you have any question or issues, please contact us:

[Steffen Roßkopf](mailto:steffen.rosskopf@student.uni-tuebingen.de)

[Sebastian Maier](mailto:sebastian.maier@student.uni-tuebingen.de)

[Cornelius Martin](mailto:cornelius.martin@student.uni-tuebingen.de)

