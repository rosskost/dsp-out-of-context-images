""" Basic configuration and settings for cosmos out-of-context model"""

import torch
from torch import nn
import tensorflow_hub as hub
import torchvision.transforms as transforms
import os

# Data Directories
# we shouldn't use a single hard coded path going forward..
BASE_DIR = os.path.join(os.getcwd(),'out_of_context_model')
DATA_DIR = os.path.join(BASE_DIR, 'data')
TARGET_DIR = os.path.join(BASE_DIR, 'viz')

# Device
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# threshould for sentence similarity and bbox overlap, change sentence similarity to 0.35.
# which was used by the COSMOS challenge people.
iou_overlap_threshold = 0.5
textual_sim_threshold = 0.35

# Word Embeddings
embedding_length = 300
embed_type = 'use'  # using universal sentence encoder embeddings for captions, see hub.load below


# instead of downloading at first start of the app (or of the dockerfile that runs the app),
# we load from this local file (980 mb big, otherwise we hat to download that at the start)
# download from here: https://tfhub.dev/google/universal-sentence-encoder/4?tf-hub-format=compressed" (unzip after)

use_embed = hub.load(os.path.join(os.getcwd(), 'saved_weights', 'universal-sentence-encoder_4'))

num_boxes = 11  # Number of bounding boxes used in experiments, one additional box for entire image (global context)

##########################################
# the rest of these parameters are used for training, so they are not needed for the inference we do.
# still need to define them, since otherwise we would have to rewrite the model definitions in
# out_of_context/model_arch

# Training Hyperparameters:
patience = 10
batch_size = 64
epochs = 500
# Optimizers
lr = 1e-3
img_lr = 1e-3
text_lr = 1e-3

# Losses
cse_loss = nn.CrossEntropyLoss(reduction='mean')
mse_loss = nn.MSELoss(reduction='mean')
margin_rank_loss = nn.MarginRankingLoss(margin=1)


# Data Pre-processing
img_transform = transforms.Compose([transforms.ToPILImage(), transforms.ToTensor(), transforms.Normalize([0.5] * 3, [0.5] * 3)])
img_transform_train = transforms.Compose([transforms.ToPILImage(), transforms.ColorJitter(hue=.2, saturation=.2), transforms.ToTensor(),  transforms.Normalize([0.5] * 3, [0.5] * 3)])

# Image Partitioning

retrieve_gt_proposal_features = True # Flag that controls whether to retrieve bounding box features from Mask RCNN backbone or not
scoring = 'dot'  # Scoring function to combine image and text embeddings
###################################################
