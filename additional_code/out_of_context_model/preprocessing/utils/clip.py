import os
from PIL import Image
from sentence_transformers import SentenceTransformer, util #needs to be updated to 2.1.0



# load clip model
clip = SentenceTransformer('clip-ViT-B-32', cache_folder=os.path.join(os.getcwd(), "saved_weights"))

seagul = r"C:\Users\srosskopf\Documents\data\test\467.jpg"

img_emb = clip.encode(Image.open(seagul))

str_emb = clip.encode(["A seagul enjoying his day",
    "Morris Leonard came across this space invader in Leven",
    "Gull in a car park in Leven"])

score = util.cos_sim(img_emb, str_emb)

print(score)
