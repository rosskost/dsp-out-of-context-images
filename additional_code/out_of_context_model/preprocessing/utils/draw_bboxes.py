import cv2
import os
from PIL import Image
import random

def draw_top_scoring_bboxes(file_path: str, bbox_1: list, bbox_2: list, drawing_mode: int = 0) -> None:
    """
    Utility functions to visualize/draw the top bounding box for each caption
    additionally to the "normal" blue ones inside the picture with bboxes,
    that we show on the '/results' page.
    Image gets saved and template picks it up from there.
    'two_comparisons' indicates if we're running inference on two (cap1,cap2) pairs.
    """
    random.seed(1)
    ### we should define consistent colours for each caption:
    # colors are ( B G R)!

    # color_1 = (0, 0, 255)  # red
    # color_2 = (0, 255, 0)  # green
    # color_3 = (0, 0, ) # black
 
    line_thickness = 15
    color_1 = (169, 57, 27)
    color_2 = (28, 132, 196)
    color_3 = (0, 51, 0)


    # read in image:
    img = cv2.imread(file_path)

    # draw bbox first caption:
    # move a little bit to show full-image box nicely
    img = cv2.rectangle(img,
            (int(bbox_1[0]) + 4, int(bbox_1[1]) + 4),
            (int(bbox_1[2]) - 4, int(bbox_1[3]) - 4),
            color_1, line_thickness)

    # for the second bbox it gets tricky:
    # because if we find a match with the uploaded picture in our database,
    # we actually compare it with both captions, that were webscraped.
    # and we also draw the boxes into the same picture:
    #
    # so we need to do some hacky stuff here to show the top bboxes for two inferences at the same time:
    # for this we define a drawing mode:
    # if drawing_mode = 0, the normal case, we just have two captions and draw them on the picture and save the picture
    # with his original name, replacing the old one.
    # if drawing_mode = 1, we need to prepare the drawing of 4 bboxes in one picture, we do this by slightly changing the
    # image name to make it distinguble and we draw the boxes again.
    # if drawing_mode = 2, we first draw the top bbox for each caption inside the picture, this time with a different color
    # for the second caption (the first caption from the user of the website stays the same).
    # Now we assume we have already saved the image from drawing_mode 1, and now we take this image and
    # blend these two pictures on top of each other with transparency of 50%. This way we see the top bboxes for
    # two inferences at the same time on the website.
    # 
    # This is very hacky and not ideal, but seems to be the only way to do this without a major refactoring and
    # changing the logic of the main code.
    
    # if drawing_mode in (1,0):
    #     colour_second_box = color_2
    #     # if the bboxes would overlap, we move them few pixels to the inside, looks way better:
    #     if  bbox_1.tolist() == bbox_2.tolist():   
    #         move_pixels = 14
    #     else:
    #         move_pixels = 6

    # else:
    #     colour_second_box = color_3
    #     if  bbox_1.tolist() == bbox_2.tolist():
    #         move_pixels = 9
    #     else:
    #         move_pixels = 6
    move_pixels = 6

    if drawing_mode == 2:
        color_second_box = color_3
        if list(bbox_1) == list(bbox_2):
            move_pixels = 12


    else:
        color_second_box = color_2
        if list(bbox_1) == list(bbox_2):
            move_pixels = 8


    img = cv2.rectangle(img,
        (int(bbox_2[0]) + move_pixels, int(bbox_2[1]) + move_pixels),
        (int(bbox_2[2]) - move_pixels, int(bbox_2[3]) - move_pixels),
        color_second_box, line_thickness - 6*drawing_mode)

    
    
    # we need to change filename of the first picture to be able to lay the two images on top of each other later:
    if drawing_mode == 1:
        head, tail = os.path.split(file_path)
        file_path = os.path.join(head, "util_1" + tail)
    
    print(file_path)
    cv2.imwrite(file_path, img)

    # after saving the second image, we need to read both images back in and blend them together
    # we need to save the second image, otherwise the conversion is not right, weirly enough
    # if can't use the non-saved array to blend, it needes to be saved and load in again.
    if drawing_mode == 2:
        head, tail = os.path.split(file_path)
        path_util_image = os.path.join(head, "util_1" + tail)
        
        img_util_I = cv2.imread(path_util_image)

        img_util_II = cv2.imread(file_path)

         # overlap the two images:
        img = cv2.addWeighted(img_util_I, 0.8, img_util_II, 0.8, 0)

        cv2.imwrite(file_path, img)
