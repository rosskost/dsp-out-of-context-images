from out_of_context_model.utils_inference.config import use_embed, device
import torch
import spacy

# load spacy entitiy:
spacy_en = spacy.load('en_core_web_sm') # need to use full name here
spacy_en.add_pipe(spacy_en.create_pipe('merge_entities'))
#spacy_en.add_pipe('merge_entities')



def process_raw_captions(caption1: str, caption2: str, return_only_emb: bool = True):

    """
    This function does Named Entity Recognition with the two input captions
    and replaces the detected objects inside the string with their Entity.
    For example: "Angela Merkel at the UNO assembly" could be converted to
    "PERSON at the ORG assembly".
    After that we embed the resulting string and return the whole embedding
    sequence as a tensor.
    Depending on the 'return_only_emb'-flag, returns the original captions as string, too.
    """
    # modify the raw captions 1 and 2 with spacy ner:

    ner = spacy_en(caption1)

    # replace the str, that get recognized as a named entitiy, with their entitiy:
    # example: 'This picture shows Tim Cook, former CEO of Apple' will be:
    #            This picture shows PERSON, former CEO of ORG

    cap1 = " ".join([t.text if not t.ent_type_ else t.ent_type_ for t in ner])
    print(cap1)

    # same thing for second caption:
    ner = spacy_en(caption2)
    cap2 = " ".join([t.text if not t.ent_type_ else t.ent_type_ for t in ner])
    print(cap2)
   
    embed_c1 = torch.tensor(use_embed([cap1]).numpy()).to(device)
    embed_c2 = torch.tensor(use_embed([cap2]).numpy()).to(device)

    if return_only_emb:

        return embed_c1, embed_c2

    # defending if we want to do the sentence-similarity (sim_score) with the original caption or
    # the caption, where the named enteties are replaced, we need to return the str with the
    # replaced names enteties as well:
    else:
        return embed_c1, embed_c2, cap1, cap2    
