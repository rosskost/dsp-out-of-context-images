###
# Script to train the fake news classifier on the BABE database
###


### Import packages
########################################################################

# Basic
import pandas as pd
import numpy as np
import torch
from torch import nn
from torch.optim import Adam
from tqdm import tqdm
import os

# Preprocessing
from transformers import BertTokenizer, BertModel

# Define pre-trained BERT model
PRE_TRAINED_MODEL_NAME = 'bert-base-uncased'


### Load data
#######################################################################

# data path
data_path = '/content/drive/My Drive/Data Science Project/0.Final/1.Data/'
model_path = '/content/drive/My Drive/Data Science Project/0.Final/2.Checkpoints/PB_BABE_best_model.bin'


# Read raw data
df = pd.read_excel(os.path.join(data_path, 'final_labels_SG1.xlsx'))
df.rename(columns={'text': 'title', 'label_bias': 'label'}, inplace=True)

# Clean labels
df = df.loc[df['label'] != 'No agreement']
df = df.loc[:, ['title', 'label']].reset_index(drop = True)

# Assigne binary labels
df.loc[df['label'] == 'Biased', 'label'] = 1
df.loc[df['label'] == 'Non-biased', 'label'] = 0



### Dataset Class and BERT tokenizer
######################################################################

MAX_LEN = 100 # based on text length

tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME)

class Dataset(torch.utils.data.Dataset):

    def __init__(self, df):

        self.labels =  df['label'].values
        self.texts = [tokenizer(text, 
                               padding='max_length', max_length = MAX_LEN, truncation=True,
                                return_tensors="pt") for text in df['title']]

    def classes(self):
        return self.labels

    def __len__(self):
        return len(self.labels)

    def get_batch_labels(self, idx):
        # Fetch a batch of labels
        return np.array(self.labels[idx])

    def get_batch_texts(self, idx):
        # Fetch a batch of inputs
        return self.texts[idx]

    def __getitem__(self, idx):

        batch_texts = self.get_batch_texts(idx)
        batch_y = self.get_batch_labels(idx)

        return batch_texts, batch_y
        

### Train test split
########################################################################

np.random.seed(112) # set seed

df_train, df_val, df_test = np.split(df.sample(frac=1, random_state=42), 
                                     [int(.8*len(df)), int(.9*len(df))])


print(f'Train label distribution: \n{df_train.label.value_counts()}')
print(f'Val label distribution: \n{df_val.label.value_counts()}')
print(f'Test label distribution: \n{df_test.label.value_counts()}')


### Define model
#######################################################################

class BertClassifier(nn.Module):

    def __init__(self, dropout=0.5):

        super(BertClassifier, self).__init__()

        self.bert = BertModel.from_pretrained("bert-base-uncased")
        self.dropout = nn.Dropout(dropout)
        self.linear = nn.Linear(self.bert.config.hidden_size, 2)
        self.sigmoid = nn.Sigmoid()

    def forward(self, input_id, mask):

        _, pooled_output = self.bert(input_ids= input_id, attention_mask=mask,return_dict=False)
        dropout_output = self.dropout(pooled_output)
        linear_output = self.linear(dropout_output)
        final_layer = self.sigmoid(linear_output)

        return final_layer
        
        

### Define training function

def train(model, train_data, val_data, learning_rate, epochs):

    # extract train and validation data
    train, val = Dataset(train_data), Dataset(val_data)

    # create train and validation loader
    train_dataloader = torch.utils.data.DataLoader(train, batch_size=16, shuffle=True)
    val_dataloader = torch.utils.data.DataLoader(val, batch_size=16)
    
    # enable GPU usage if available
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    # define criterion and optimizer
    criterion = nn.CrossEntropyLoss()
    optimizer = Adam(model.parameters(), lr= learning_rate)


    if use_cuda:

            model = model.cuda()
            criterion = criterion.cuda()

    for epoch_num in range(epochs):

            total_acc_train = 0
            total_loss_train = 0

            for train_input, train_label in tqdm(train_dataloader):

                train_label = train_label.to(device)
                mask = train_input['attention_mask'].to(device)
                input_id = train_input['input_ids'].squeeze(1).to(device)

                output = model(input_id, mask)
                
                batch_loss = criterion(output, train_label)
                total_loss_train += batch_loss.item()
                
                acc = (output.argmax(dim=1) == train_label).sum().item()
                total_acc_train += acc

                model.zero_grad()
                batch_loss.backward()
                optimizer.step()
            
            total_acc_val = 0
            total_loss_val = 0

            with torch.no_grad():

                for val_input, val_label in val_dataloader:

                    val_label = val_label.to(device)
                    mask = val_input['attention_mask'].to(device)
                    input_id = val_input['input_ids'].squeeze(1).to(device)

                    output = model(input_id, mask)

                    batch_loss = criterion(output, val_label)
                    total_loss_val += batch_loss.item()
                    
                    acc = (output.argmax(dim=1) == val_label).sum().item()
                    total_acc_val += acc
            
            print(
                f'Epochs: {epoch_num + 1} | Train Loss: {total_loss_train / len(train_data): .3f} \
                | Train Accuracy: {total_acc_train / len(train_data): .3f} \
                | Val Loss: {total_loss_val / len(val_data): .3f} \
                | Val Accuracy: {total_acc_val / len(val_data): .3f}')
            
    # Save        
    #torch.save(model.state_dict(), model_path)
             

# Define parameters for training                 
EPOCHS = 3
LR = 2e-5

# Call instance of model
model = BertClassifier()

# train model              
train(model, df_train, df_val, LR, EPOCHS)


### Define evalution function
#############################

def evaluate(model, test_data):
    
    # extract test data
    test = Dataset(test_data)
    
    # create dataloader for test data
    test_dataloader = torch.utils.data.DataLoader(test, batch_size=2)

    # enable GPU usage if available
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    if use_cuda:

        model = model.cuda()

    total_acc_test = 0
    with torch.no_grad():

        for test_input, test_label in test_dataloader:

              test_label = test_label.to(device)
              mask = test_input['attention_mask'].to(device)
              input_id = test_input['input_ids'].squeeze(1).to(device)

              output = model(input_id, mask)

              acc = (output.argmax(dim=1) == test_label).sum().item()
              total_acc_test += acc
    
    print(f'Test Accuracy: {total_acc_test / len(test_data): .3f}')
 
# model evaluation   
evaluate(model, df_test)
