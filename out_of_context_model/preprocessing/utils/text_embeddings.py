from typing import Tuple
import spacy

# load spacy entitiy:
spacy_en = spacy.load('en_core_web_sm') # need to use full name here
spacy_en.add_pipe(spacy_en.create_pipe('merge_entities'))
#spacy_en.add_pipe('merge_entities')



def process_raw_captions(caption1: str, caption2: str) -> Tuple[str, str]:

    """
    This function does Named Entity Recognition with the two input captions
    and replaces the detected objects inside the string with their Entity.
    For example: "Angela Merkel at the UNO assembly" could be converted to
    "PERSON at the ORG assembly".
    """
    # modify the raw captions 1 and 2 with spacy ner:

    ner = spacy_en(caption1)

    # replace the str, that get recognized as a named entitiy, with their entitiy:
    # example: 'This picture shows Tim Cook, former CEO of Apple' will be:
    #            This picture shows PERSON, former CEO of ORG

    cap1 = " ".join([t.text if not t.ent_type_ else t.ent_type_ for t in ner])
    print(cap1)

    # same thing for second caption:
    ner = spacy_en(caption2)
    cap2 = " ".join([t.text if not t.ent_type_ else t.ent_type_ for t in ner])
    print(cap2)

    return cap1, cap2
    