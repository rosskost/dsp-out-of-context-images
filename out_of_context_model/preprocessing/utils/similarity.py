import os
# if problems with import scipy for SentenceTransfromers:
# (downgraded to 1.4.1) and fill _distributor_init.py see: https://github.com/scipy/scipy/issues/11826
from sentence_transformers import SentenceTransformer, util

# all-mpnet-base-v2
model_sim = SentenceTransformer(
        "sentence-transformers/all-MiniLM-L6-v2",
        device="cpu",
        cache_folder=os.path.join(os.getcwd(), 'saved_weights'))


def get_bert_sim_scores(caption1: str, caption2: str) -> float:
    """"
    This function returns the sentence similarity between the two given input captions.
    We do this with the 'sentence-transformers/all-MiniLM-L6-v2' net, which I thought is a good
    choice here, and as similarity function the classic cosine similarity.
    """

    _embeddings_1 = model_sim.encode(caption1, convert_to_tensor=True)
    _embeddings_2 = model_sim.encode(caption2, convert_to_tensor=True)

    return util.cos_sim(_embeddings_1, _embeddings_2).item()
