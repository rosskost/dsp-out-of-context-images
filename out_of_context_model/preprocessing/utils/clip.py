import os
from typing import Tuple, List
from PIL import Image
from sentence_transformers import SentenceTransformer, util

# load clip model
clip = SentenceTransformer('clip-ViT-B-32', cache_folder=os.path.join(os.getcwd(), "saved_weights"))


def get_clip_scores_bboxes(bbox_list: list, caption_1: str, caption_2: str, image_path: str) -> Tuple[List[float], List[float]]:
    """
    
    
    
    """
    # load in image:
    img = Image.open(os.path.join(image_path))

    # store image embeddings in list
    img_embs = []

    # we cut out all the bboxes and get a clip embedding just for this small bbox region.
    for bbox in bbox_list:
        img_cropped = img.crop(bbox)
        img_embs.append(clip.encode(img_cropped))

    # get clip embeddings for both captions:
    cap_1_enc =  clip.encode(caption_1)
    cap_2_enc =  clip.encode(caption_2)

    # via cos_sim, get a score for the similarity of the cap embedding and each bbox region.
    # scores is a list with 11 float values (0,1) for each box (11th box whole image)
    scores_1 = util.cos_sim(cap_1_enc, img_embs).squeeze().tolist()
    scores_2 = util.cos_sim(cap_2_enc, img_embs).squeeze().tolist()


    print(scores_1)
    print(scores_2)

    return scores_1, scores_2
