import os
import cv2
from typing import Tuple, List
from imageai.Detection import ObjectDetection

detector = ObjectDetection()

detector.setModelTypeAsRetinaNet()
# expects weights in ./saved_weights/
detector.setModelPath(os.path.join(os.getcwd(), "saved_weights", "resnet50_coco_best_v2.1.0.h5"))

detector.loadModel(detection_speed="normal") # can set detection_speed here which leads to slight lose of accuracy,
# default = "normal", options: “normal”, “fast”, “faster”, “fastest” “flash”.


def get_bounding_boxes_imageai(
        image_filepath: str, max_number_boxes_in_image: int=10, return_image_as_file: bool = True,
        execution_path: str = None) -> Tuple[List, str]:
    """
    This function takes as input:
    1) a str filepath to an image, it should detect bboxes on.
    2) the max number of boxes (default = 10 should almost never be changed).
    3) a bool value, that decides if the function returns the processed image as numpy array or saves it in a folder
    4) an execution path, where the images should be saved, default to None, which is a good default
    in almost all our cases
    
    retuns:
        - the detected 10 bbox classes + the 11th full image bbox as a list of tensors
        - depending on  input 3), the input image with drawn bboxes as a numpy array
            or the full path of the saved image.
    """

    if execution_path == None:
        execution_path = os.getcwd()

    else:
        pass


    #########################################################################
    # the paramter "max_number_boxes_in_image" is not supported by the module,
    # I changed the source code here to only draw the top ten boxes, otherwise it would
    # look clustered depending on the minimum_percentage_probability.
    # I did this because I always want the option to extract ten boxes, for which I need a
    # lower min probability, but depending on the picture (some have obvious and a lot of objects,
    # some barely five), this may look clustered when I draw the boxes onto the image.
    # need to rember this when we move to another machine and install the default imageai package:
    # changes in: {venv} -> site_packages/imageai/Detection/__init__().py
    ##########################################################################

    folder_images_boxes = "test_images_with_boxes"

    if return_image_as_file:
        if not os.path.exists(os.path.join(execution_path, folder_images_boxes)):
            os.makedirs(os.path.join(execution_path, folder_images_boxes))
            print(f"created folder : {folder_images_boxes}")
        else:
            pass   # if folder, in which we put the images with drawn boxes, exists,
                   # do nothing here

        path_saved_image = os.path.join(
                execution_path,folder_images_boxes,"with_bboxes_"+image_filepath.split("/")[-1])

        detections = detector.detectObjectsFromImage(input_image=image_filepath, output_type="file",
                output_image_path=path_saved_image,
                minimum_percentage_probability=0,
                max_number_boxes_in_image=max_number_boxes_in_image,
                display_object_name=False)

    else:
        returned_image, detections = detector.detectObjectsFromImage(input_image=image_filepath,
            output_type="array",
            minimum_percentage_probability=0,
            max_number_boxes_in_image=max_number_boxes_in_image,
            display_object_name=False)

    # at max 10 boxes:
    bbox_list = [i["box_points"] for i in detections[0:10]]
    
    # read in image to get shape:
    img = cv2.imread(image_filepath)

    img_shape = img.shape[:2]


    # append bbox of entire image as 11th bbox:
    bbox_list.append([0, 0, img_shape[1], img_shape[0]])  # For entire image (global context)

    if return_image_as_file:
        return bbox_list, path_saved_image

    else:
        return bbox_list, returned_image
