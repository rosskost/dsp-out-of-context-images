from out_of_context_model.utils_inference.config import *
from out_of_context_model.utils_inference.eval_utils import is_bbox_overlap, top_bbox_from_scores
from out_of_context_model.preprocessing.utils.text_embeddings import process_raw_captions
from out_of_context_model.preprocessing.utils.bboxes_pre import get_bounding_boxes_imageai
from out_of_context_model.preprocessing.utils.similarity import get_bert_sim_scores
from out_of_context_model.preprocessing.utils.draw_bboxes import draw_top_scoring_bboxes
from out_of_context_model.preprocessing.utils.clip import get_clip_scores_bboxes
from typing import Tuple
import numpy as np

def get_prediction_website_input(imagepath: str, caption1: str, caption2: str, sim_on_ner_caps: bool = False,
        text_emb_on_ner: bool = False, drawing_mode: int = 0) -> Tuple[int, float]:
    """
    This function calls the whole pipeline to get a predictions from the COSMOS-model,
    if we have (image, cap1, cap2) as input from the website.
    We call all the preprocessing functions and fed the preprocessed inputs to
    the cosmos model to retrun a prediction: 0: in context, 1: out-of-context.
    Aditionally we draw the bboxes on the input image.
    """

    ner_str_1, ner_str_2 = process_raw_captions(caption1, caption2)

 
    bboxes, path_bbox_image = get_bounding_boxes_imageai(imagepath, 10, return_image_as_file=True)
    # need a check if we've got 10 boxes (+1 for the whole image),
    # otherwise we get an error in the forward function:
    print(bboxes)
    if len(bboxes) != 11:
        raise ValueError(f"Warning: Couldn't detect enough boxes for {imagepath}, skipping it..... ")
    
    # here we compute the similarity score (0,1) between the two captions:
    if sim_on_ner_caps:
        sim_score = get_bert_sim_scores(ner_str_1, ner_str_2)

    else:
        sim_score = get_bert_sim_scores(caption1, caption2)

    print(sim_score)

    # get pure language model context, not really needed atm, but maybe useful:
    _language_context = 0 if sim_score >= textual_sim_threshold else 1 
    

    if text_emb_on_ner:
        scores_c1, scores_c2 = get_clip_scores_bboxes(bboxes, ner_str_1, ner_str_2, path_bbox_image)
    else:
        scores_c1, scores_c2 = get_clip_scores_bboxes(bboxes, caption1, caption2, path_bbox_image)
    
    # inside this function we zip the bounding boxes with their corresponding score,
    # order them after scores and return the top scoring box, which is the one
    # that is most associated with the caption.
    top_bbox_c1 = bboxes[np.argmax(scores_c1)]
    top_bbox_c2 = bboxes[np.argmax(scores_c2)]

    print(top_bbox_c1)
    print(top_bbox_c2)

    # now that we have two best bboxes, draw them on input picture:
    draw_top_scoring_bboxes(path_bbox_image, top_bbox_c1, top_bbox_c2, drawing_mode)

    # here we simply test if the two bboxes overlap or not.
    # threshold for Intersection over Union in config, we're using 0.5
    bbox_overlap = is_bbox_overlap(top_bbox_c1, top_bbox_c2, iou_overlap_threshold)

    if bbox_overlap:
            # if they overlap -> refer to same object
        if sim_score >= textual_sim_threshold:  # 0.35
            pred_context = 0 # if they refer to the same object and have a high enough sentence similarity (0.35),
            # we predict them as in context.
        else:
            # if they overlap and are not similar enough, out-of-context
            pred_context = 1
    else:
        # if sim score very low and they don't refer to the same box (ioU < 0.5) we should probably still
        # make some saving mechanism here, to avoid completly contrary captions from being shown as in context.
        if sim_score < 0.05:
            pred_context = 1
        else:
            # otherwise if they don't overlap, classify as in context.
            pred_context = 0

    return pred_context, sim_score
