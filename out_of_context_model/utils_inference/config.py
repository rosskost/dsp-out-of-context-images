""" Basic configuration and settings for cosmos out-of-context model"""

import torch
from torch import nn
import torchvision.transforms as transforms
import os

# Data Directories
# we shouldn't use a single hard coded path going forward..
BASE_DIR = os.path.join(os.getcwd(),'out_of_context_model')
DATA_DIR = os.path.join(BASE_DIR, 'data')
TARGET_DIR = os.path.join(BASE_DIR, 'viz')

# Device
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# threshould for sentence similarity and bbox overlap, change sentence similarity to 0.35.
# which was used by the COSMOS challenge people.
iou_overlap_threshold = 0.5
textual_sim_threshold = 0.35

# Word Embeddings
embedding_length = 300
embed_type = 'use'  # using universal sentence encoder embeddings for captions, see hub.load below


retrieve_gt_proposal_features = True # Flag that controls whether to retrieve bounding box features from Mask RCNN backbone or not
num_boxes = 11  # Number of bounding boxes used in experiments, one additional box for entire image (global context)

# Image threshold to determine , if we map a costum user image to one in our database:
image_threshold = 0.15
