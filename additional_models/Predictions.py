import os
import torch
from torch import nn

# Preprocessing
from transformers import BertTokenizer, BertModel

# Pretrained Bert Model for preprocessing
PRE_TRAINED_MODEL_NAME = 'bert-base-uncased'


model_path = os.path.join(os.getcwd(), "additional_models")
bert_tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME)

class BertClassifier(nn.Module):

    def __init__(self, dropout=0.5):

        super(BertClassifier, self).__init__()

        self.bert = BertModel.from_pretrained(PRE_TRAINED_MODEL_NAME)
        self.dropout = nn.Dropout(dropout)
        self.linear = nn.Linear(self.bert.config.hidden_size, 2)
        #self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def forward(self, input_id, mask):

        _, pooled_output = self.bert(input_ids= input_id, attention_mask=mask,return_dict=False)
        dropout_output = self.dropout(pooled_output)
        linear_output = self.linear(dropout_output)
        final_layer = self.sigmoid(linear_output)
        #self.relu(linear_output)

        return final_layer

### Bert Model
#model_FakeNewsNet = BertClassifier()
#model_Covid = BertClassifier()
#model_Fakeddit = BertClassifier()
#model_Nela = BertClassifier()
#model_Hate = BertClassifier()

#model_FakeNewsNet.load_state_dict(torch.load(os.path.join(model_path, 'FN_FNN_best_model.bin'), map_location=torch.device('cpu')))
#model_Covid.load_state_dict(torch.load(os.path.join(model_path, 'FN_Covid_best_model.bin'), map_location=torch.device('cpu')))
#model_Fakeddit.load_state_dict(torch.load(os.path.join(model_path, 'FN_Fakeddit_best_model.bin'), map_location=torch.device('cpu')))
#model_Nela.load_state_dict(torch.load(os.path.join(model_path, 'FN_nela_best_model.bin'), map_location=torch.device('cpu')))
#model_Hate.load_state_dict(torch.load(os.path.join(model_path, 'HS_Twitter_best_model.bin'), map_location = torch.device('cpu')))
