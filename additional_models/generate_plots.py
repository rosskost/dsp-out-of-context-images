import os
from . import Predictions
import matplotlib.pyplot as plt
from sentence_transformers import util
from out_of_context_model.preprocessing.utils.similarity import model_sim
import numpy as np

def get_spider_chart_one(caption1):
    ### Model input
    ###############

    input_title = caption1

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")


    ### Extract features for BERT Models
    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    ### Output of classifiers

    out_FNN = Predictions.model_FakeNewsNet(input_id, mask).select(1,1).detach().numpy()
    out_Covid = Predictions.model_Covid(input_id, mask).select(1,1).detach().numpy()
    out_Fakeddit = Predictions.model_Fakeddit(input_id, mask).select(1,1).detach().numpy()
    out_Nela = Predictions.model_Nela(input_id, mask).select(1,1).detach().numpy()
    out_Hate = Predictions.model_Hate(input_id, mask).select(1,1).detach().numpy()


    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001


    categories = ['FNN', 'Covid', 'Fakeddit', 'Nela', 'Hate']
    categories = [*categories, categories[0]]

    scores_caption1 = [out_FNN[0], out_Covid[0], out_Fakeddit[0], out_Nela[0], out_Hate[0]]
    scores_caption1 = [*scores_caption1, scores_caption1[0]]

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(scores_caption1))

    plot = plt.figure(figsize=(8, 8))
    plt.subplot(polar=True)
    plt.plot(label_loc, scores_caption1)

    plt.title('Fake News Classifiers - Fake News Scores', size=20)
    lines, labels = plt.thetagrids(np.degrees(label_loc), labels=categories)
    plt.legend()

    return plot

def get_spider_chart_two(caption1, caption2):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")


    ### Extract features for BERT Models
    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    ### Output of classifiers

    out_FNN = Predictions.model_FakeNewsNet(input_id, mask).select(1,1).detach().numpy()
    out_Covid = Predictions.model_Covid(input_id, mask).select(1,1).detach().numpy()
    out_Fakeddit = Predictions.model_Fakeddit(input_id, mask).select(1,1).detach().numpy()
    out_Nela = Predictions.model_Nela(input_id, mask).select(1,1).detach().numpy()
    out_Hate = Predictions.model_Hate(input_id, mask).select(1,1).detach().numpy()


    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001


    categories = ['FNN', 'Covid', 'Fakeddit', 'Nela', 'Hate']
    categories = [*categories, categories[0]]

    scores_caption1 = [out_FNN[0], out_Covid[0], out_Fakeddit[0], out_Nela[0], out_Hate[0]]
    scores_caption1 = [*scores_caption1, scores_caption1[0]]

    scores_caption2 = [out_FNN[1], out_Covid[1], out_Fakeddit[1], out_Nela[1], out_Hate[0]]
    scores_caption2 = [*scores_caption2, scores_caption2[0]]

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(scores_caption1))

    plot = plt.figure(figsize=(8, 8))
    plt.subplot(polar=True)
    plt.plot(label_loc, scores_caption1, label = 'Caption 1')
    plt.plot(label_loc, scores_caption2, label = 'Caption 2')

    plt.title('Fake News Classifiers - Fake News Scores', size=20)
    lines, labels = plt.thetagrids(np.degrees(label_loc), labels=categories)
    plt.legend()

    return plot

def get_spider_chart_three(caption1, caption2, caption3):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2
    input_title_3 = caption3

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")


    ### Extract features for BERT Models
    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    ### Output of classifiers

    out_FNN = Predictions.model_FakeNewsNet(input_id, mask).select(1,1).detach().numpy()
    out_Covid = Predictions.model_Covid(input_id, mask).select(1,1).detach().numpy()
    out_Fakeddit = Predictions.model_Fakeddit(input_id, mask).select(1,1).detach().numpy()
    out_Nela = Predictions.model_Nela(input_id, mask).select(1,1).detach().numpy()
    out_Hate = Predictions.model_Hate(input_id, mask).select(1,1).detach().numpy()


    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2, input_title_3]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2, input_title_3]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001


    categories = ['FNN', 'Covid', 'Fakeddit', 'Nela', 'Hate']
    categories = [*categories, categories[0]]

    scores_caption1 = [out_FNN[0], out_Covid[0], out_Fakeddit[0], out_Nela[0], out_Hate[0]]
    scores_caption1 = [*scores_caption1, scores_caption1[0]]

    scores_caption2 = [out_FNN[1], out_Covid[1], out_Fakeddit[1], out_Nela[1], out_Hate[0]]
    scores_caption2 = [*scores_caption2, scores_caption2[0]]

    scores_caption3 = [out_FNN[2], out_Covid[2], out_Fakeddit[2], out_Nela[2], out_Hate[0]]
    scores_caption3 = [*scores_caption3, scores_caption3[0]]

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(scores_caption1))

    plot = plt.figure(figsize=(8, 8))
    plt.subplot(polar=True)
    plt.plot(label_loc, scores_caption1, label = 'Your Caption')
    plt.plot(label_loc, scores_caption2, label = 'Original Caption 1')
    plt.plot(label_loc, scores_caption3, label = 'Original Caption 2')

    plt.title('Fake News Classifiers - Fake News Scores', size=20)
    lines, labels = plt.thetagrids(np.degrees(label_loc), labels=categories)
    plt.legend()

    return plot



def save_fake_bar_chart_one(caption1, save_as):
    ### Model input
    ###############

    input_title = caption1

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Caption 1']
    c_scores_fake = [cs_fake[0]]
    ax.bar(s, c_scores_fake, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 0.2, ls = 'dotted', label = 'fake boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_fake_bar_chart_two(caption1, caption2, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Caption 1', 'Caption 2']
    c_scores_fake = [cs_fake[0], cs_fake[1]]
    ax.bar(s, c_scores_fake, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 1.2, ls = 'dotted', label = 'fake boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_fake_bar_chart_three(caption1, caption2, caption3, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2
    input_title_3 = caption3

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2, input_title_3]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2, input_title_3]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Your Caption', 'Original Caption 1', 'Original Caption 2']
    c_scores_fake = [cs_fake[0], cs_fake[1], cs_fake[2]]
    ax.bar(s, c_scores_fake, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 2.2, ls = 'dotted', label = 'fake boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))



def save_hate_bar_chart_one(caption1, save_as):
    ### Model input
    ###############

    input_title = caption1

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Caption 1']
    c_scores_hate = [cs_hate[0]]
    ax.bar(s, c_scores_hate, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 0.2, ls = 'dotted', label = 'hate boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_hate_bar_chart_two(caption1, caption2, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Caption 1', 'Caption 2']
    c_scores_hate = [cs_hate[0], cs_hate[1]]
    ax.bar(s, c_scores_hate, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 1.2, ls = 'dotted', label = 'hate boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_hate_bar_chart_three(caption1, caption2, caption3, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2
    input_title_3 = caption3

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    tok = Predictions.bert_tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")

    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    sen_fake = [sentence_fake, input_title, input_title_2, input_title_3]
    emb_fake = model_sim.encode(sen_fake)

    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    sen_hate = [sentence_hate, input_title, input_title_2, input_title_3]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_fake = util.cos_sim([emb_fake[0]], emb_fake[1:])[0]
    cs_hate = util.cos_sim([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001


    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    fig = plt.figure()
    ax = fig.add_axes([0,0,1,1])
    s = ['Your Caption', 'Original Caption 1', 'Original Caption 2']
    c_scores_hate = [cs_hate[0], cs_hate[1], cs_hate[2]]
    ax.bar(s, c_scores_hate, width = 0.25, color = 'grey')
    ax.hlines(0.15, xmin = -0.2, xmax = 2.2, ls = 'dotted', label = 'hate boundary')
    fig.legend()
    extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))