import os
from . import Predictions_addons
from out_of_context_model.preprocessing.utils.similarity import model_sim
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib.pyplot as plt



def save_fake_news_spdr(caption1, caption2, caption3, save_as):

    ### Model input
    ###############

    input_title_1 = caption1
    input_title_2 = caption2
    if caption3 != "none":
        input_title_3  = caption3

    ### Preprocess input
    ####################
    if caption3 != "none":
        tok = Predictions_addons.tokenizer([input_title_1, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
        truncation=True, return_tensors="pt")
    else:
        tok = Predictions_addons.tokenizer([input_title_1, input_title_2] ,padding='max_length', max_length = 40, 
        truncation=True, return_tensors="pt")

    ### Extract features for BERT Models
    ####################################
    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    ### Output of classifiers
    ### Fake

    out_FNN = Predictions_addons.model_FakeNewsNet(input_id, mask).select(1,1).detach().numpy()
    #out_Covid = Predictions_addons.model_Covid(input_id, mask).select(1,1).detach().numpy()
    out_Fakeddit = Predictions_addons.model_Fakeddit(input_id, mask).select(1,1).detach().numpy()
    out_Nela = Predictions_addons.model_Nela(input_id, mask).select(1,1).detach().numpy()

    ### Media Bias
    #out_MediaBias = Predictions_addons.model_MediaBias(input_id, mask).select(1,1).detach().numpy()

    categories =['FakeNewsNet', 'Fakeddit', 'NELA_GT']
    categories = [*categories, categories[0]]

    caption_1 = [out_FNN[0], out_Fakeddit[0], out_Nela[0]]
    caption_2 = [out_FNN[1], out_Fakeddit[1], out_Nela[1]]
    caption_1 = [*caption_1, caption_1[0]]
    caption_2 = [*caption_2, caption_2[0]]
    if caption3 != "none":
        caption_3 = [out_FNN[2], out_Fakeddit[2], out_Nela[2]]
        caption_3 = [*caption_3, caption_3[0]]

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(caption_1))

    if caption3 != "none":
        plot = plt.figure(figsize=(8, 8))
        plt.subplot(polar=True)
        plt.plot(label_loc, caption_1, label = 'Your Caption', linewidth = 8, color = '#1B39A9', alpha = 0.7) # Blue
        plt.fill_between(label_loc, caption_1, alpha = 0.2)
        plt.plot(label_loc, caption_2, label = 'Verified Caption 1', linewidth = 8, color = '#C4841C', alpha = 0.7) # Orange
        plt.fill_between(label_loc, caption_2, alpha = 0.2)
        plt.plot(label_loc, caption_3, label = 'Verified Caption 2', linewidth = 8, color = '#46AE25', alpha = 0.7) # Green
        plt.fill_between(label_loc, caption_3, alpha = 0.2)

        lines, labels = plt.thetagrids(np.degrees(label_loc), labels = categories)
        plt.legend()
        plt.savefig(save_as)
    else:
        plot = plt.figure(figsize=(8, 8))
        plt.subplot(polar=True)
        plt.plot(label_loc, caption_1, label = 'Your first Caption', linewidth = 8, color = '#1B39A9', alpha = 0.7) # Blue
        plt.fill_between(label_loc, caption_1, alpha = 0.2)
        plt.plot(label_loc, caption_2, label = 'Your second Caption', linewidth = 8, color = '#C4841C', alpha = 0.7) # Orange
        plt.fill_between(label_loc, caption_2, alpha = 0.2)

        lines, labels = plt.thetagrids(np.degrees(label_loc), labels = categories)
        plt.legend()
        plt.savefig(save_as)

def save_hate_spdr(caption1, caption2, caption3, save_as):

    ### Model input
    ###############

    input_title_1 = caption1
    input_title_2 = caption2
    if caption3 != "none":
        input_title_3  = caption3

    #input_title = "More than one incapable politician at conference deciding on covid restrictions"
    #input_title_2 = "Former senator on tv show discussing current climate related policy"


    ### Preprocess input
    ####################
    if caption3 != "none":
        tok = Predictions_addons.tokenizer([input_title_1, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                        truncation=True, return_tensors="pt")
    else:
        tok = Predictions_addons.tokenizer([input_title_1, input_title_2] ,padding='max_length', max_length = 40, 
                        truncation=True, return_tensors="pt")        


    ### Extract features for BERT Models
    ####################################
    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    ### Hate 
    ########

    ### Self trained
    out_Hate = Predictions_addons.model_Hate(input_id, mask).select(1,1).detach().numpy()

    ### Hugging Face 

    out_hate_twitter = np.zeros(3)

    result_1 = Predictions_addons.model_offensive_twitter.classify_text(input_title_1)
    result_2 = Predictions_addons.model_offensive_twitter.classify_text(input_title_2)
    if caption3 != "none":
        result_3 = Predictions_addons.model_offensive_twitter.classify_text(input_title_3)



    if result_1.label == 'LABEL_1':
        out_hate_twitter[0] = result_1.score
    else:
        out_hate_twitter[0] = 1 - result_1.score

    if result_2.label == 'LABEL_1':
        out_hate_twitter[1] = result_2.score
    else:
        out_hate_twitter[1] = 1 - result_2.score

    if caption3 != "none":
        if result_3.label == 'LABEL_1':
            out_hate_twitter[2] = result_3.score
        else:
            out_hate_twitter[2] = 1 - result_3.score


    out_HateExplain = np.zeros(3)
    result_1 = Predictions_addons.model_hate_explain.classify_text(input_title_1)
    result_2 = Predictions_addons.model_hate_explain.classify_text(input_title_2)
    if caption3 != "none":
        result_3 = Predictions_addons.model_hate_explain.classify_text(input_title_3)

    if result_1.label == 'ABUSIVE':
        out_HateExplain[0] = result_1.score
    else:
        out_HateExplain[0] = 1 - result_1.score

    if result_2.label == 'ABUSIVE':
        out_HateExplain[1] = result_2.score
    else:
        out_HateExplain[1] = 1 - result_2.score

    if caption3 != "none":
        if result_3.label == 'ABUSIVE':
            out_HateExplain[2] = result_3.score
        else:
            out_HateExplain[2] = 1 - result_3.score

    ### Media Bias
    #out_MediaBias = Predictions_addons.model_MediaBias(input_id, mask).select(1,1).detach().numpy()

    categories =['Civil Comments', 'TwitterHate', 'HateExplain']
    categories = [*categories, categories[0]]

    caption_1 = [out_Hate[0], out_hate_twitter[0], out_HateExplain[0]]
    caption_2 = [out_Hate[1], out_hate_twitter[1], out_HateExplain[1]]
    caption_1 = [*caption_1, caption_1[0]]
    caption_2 = [*caption_2, caption_2[0]]
    if caption3 != "none":
        caption_3 = [out_Hate[2], out_hate_twitter[2], out_HateExplain[2]]
        caption_3 = [*caption_3, caption_3[0]]

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(caption_1))

    if caption3 != "none":
        plot = plt.figure(figsize=(8, 8))
        plt.subplot(polar=True)
        plt.ylim([0, 1.0])
        plt.plot(label_loc, caption_1, label = 'Your Caption', linewidth = 8, color = '#1B39A9', alpha = 0.7) # Blue
        plt.fill_between(label_loc, caption_1, alpha = 0.2)
        plt.plot(label_loc, caption_2, label = 'Verified Caption 1', linewidth = 8, color = '#C4841C', alpha = 0.7) # Orange
        plt.fill_between(label_loc, caption_2, alpha = 0.2)
        plt.plot(label_loc, caption_3, label = 'Verified Caption 2', linewidth = 8, color = '#46AE25', alpha = 0.7) # Green
        plt.fill_between(label_loc, caption_3, alpha = 0.2)

        lines, labels = plt.thetagrids(np.degrees(label_loc), labels = categories)
        plt.legend()
        plt.savefig(save_as)
    else:
        plot = plt.figure(figsize=(8, 8))
        plt.subplot(polar=True)
        plt.ylim([0, 1.0])
        plt.plot(label_loc, caption_1, label = 'Your first Caption', linewidth = 8, color = '#1B39A9', alpha = 0.7) # Blue
        plt.fill_between(label_loc, caption_1, alpha = 0.2)
        plt.plot(label_loc, caption_2, label = 'Your second Caption', linewidth = 8, color = '#C4841C', alpha = 0.7) # Orange
        plt.fill_between(label_loc, caption_2, alpha = 0.2)

        lines, labels = plt.thetagrids(np.degrees(label_loc), labels = categories)
        plt.legend()
        plt.savefig(save_as)

def save_fake_bar_chart(caption1, caption2, caption3, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2
    if caption3 != "none":
        input_title_3 = caption3

    ### Preprocess input
    if caption3 != "none":
        tok = Predictions_addons.tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")
    else:
        tok = Predictions_addons.tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")
            
    # SBERT
    sentence_fake = "fake, hoax, fabrication, supposedly, falsification, propaganda, deflection, deception, contradicted, defamation, lie, misleading, deceive, fraud, concocted, bluffing, made up, double meaning, alternative facts, trick, half-truth, untruth, falsehoods, inaccurate, disinformation, misconception"
    if caption3 != "none":
        sen_fake = [sentence_fake, input_title, input_title_2, input_title_3]
    else:
        sen_fake = [sentence_fake, input_title, input_title_2]
    emb_fake = model_sim.encode(sen_fake)

    # Cosine Similarity
    cs_fake = cosine_similarity([emb_fake[0]], emb_fake[1:])[0]

    for i in range(len(cs_fake)):
      if cs_fake[i] < 0:
        cs_fake[i] = 0.0001

    if caption3 != "none":
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your Caption', 'Verified Caption 1', 'Verified Caption 2']
        c_scores_fake = [cs_fake[0], cs_fake[1], cs_fake[2]]
        ax.bar(s, c_scores_fake, width = 0.25, color = ['#1B39A9', '#C4841C', '#46AE25'])
        ax.hlines(0.15, xmin = -0.2, xmax = 2.2, ls = 'dotted', label = 'fake boundary')
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))
    else:
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your first Caption', 'Your second Caption']
        c_scores_fake = [cs_fake[0], cs_fake[1]]
        ax.bar(s, c_scores_fake, width = 0.25, color = ['#1B39A9', '#C4841C'])
        ax.hlines(0.15, xmin = -0.2, xmax = 1.2, ls = 'dotted', label = 'fake boundary')
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_hate_bar_chart(caption1, caption2, caption3, save_as):
    ### Model input
    ###############

    input_title = caption1
    input_title_2  = caption2
    if caption3 != "none":
        input_title_3 = caption3

    ### Preprocess input
    if caption3 != "none":
        tok = Predictions_addons.tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")
    else:
        tok = Predictions_addons.tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                           truncation=True, return_tensors="pt")
            
    # SBERT
    sentence_hate = "white, black, racist, guy, hate, lie, stupid, show, anti, war, sexual, crime, rediculous, feel, poor, run, ignorant, nonsense, liar, hard, sad, loser, abuse, disgusting, ignorance, blame"
    if caption3 != "none":
        sen_hate = [sentence_hate, input_title, input_title_2, input_title_3]
    else:
        sen_hate = [sentence_hate, input_title, input_title_2]
    emb_hate = model_sim.encode(sen_hate)

    # Cosine Similarity
    cs_hate = cosine_similarity([emb_hate[0]], emb_hate[1:])[0]

    for i in range(len(cs_hate)):
      if cs_hate[i] < 0:
        cs_hate[i] = 0.0001

    if caption3 != "none":
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your Caption', 'Verified Caption 1', 'Verified Caption 2']
        c_scores_fake = [cs_hate[0], cs_hate[1], cs_hate[2]]
        ax.bar(s, c_scores_fake, width = 0.25, color = ['#1B39A9', '#C4841C', '#46AE25'])
        ax.hlines(0.15, xmin = -0.2, xmax = 2.2, ls = 'dotted', label = 'hate boundary')
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))
    else:
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your first Caption', 'Your second Caption']
        c_scores_fake = [cs_hate[0], cs_hate[1]]
        ax.bar(s, c_scores_fake, width = 0.25, color = ['#1B39A9', '#C4841C'])
        ax.hlines(0.15, xmin = -0.2, xmax = 1.2, ls = 'dotted', label = 'hate boundary')
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))

def save_media_bias(caption1, caption2, caption3, save_as):
    ### Model input
    ###############
    input_title = caption1
    input_title_2  = caption2
    if caption3 != "none":
        input_title_3 = caption3

    if caption3 != "none":
        tok = Predictions_addons.tokenizer([input_title, input_title_2, input_title_3] ,padding='max_length', max_length = 40, 
                       truncation=True, return_tensors="pt")
    else:
        tok = Predictions_addons.tokenizer([input_title, input_title_2] ,padding='max_length', max_length = 40, 
                       truncation=True, return_tensors="pt")

    mask = tok['attention_mask']
    input_id = tok['input_ids'].squeeze(1)

    out_MediaBias = Predictions_addons.model_MediaBias(input_id, mask).select(1,1).detach().numpy()

    if caption3 != "none":
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your Caption', 'Verified Caption 1', 'Verified Caption 2']
        c_scores_bias = [out_MediaBias[0], out_MediaBias[1], out_MediaBias[2]]
        ax.bar(s, c_scores_bias, width = 0.25, color = ['#1B39A9', '#C4841C', '#46AE25'])
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))
    else:
        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        s = ['Your first Caption', 'Your second Caption']
        c_scores_bias = [out_MediaBias[0], out_MediaBias[1]]
        ax.bar(s, c_scores_bias, width = 0.25, color = ['#1B39A9', '#C4841C'])
        fig.legend()
        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save_as, bbox_inches = extent.expanded(1.2, 1.2))