# [can't be done inside the repo, here are huge model weight files not present]

# to build this Dockerfile in out-of-context base folder:
# ´$ docker build -t name:tag .´   (the dot searches for a Dockerfile in the current dir)

# FROM -> suitable base image (with python 3.7.12)
FROM python:3.7.12-slim

# copy full working directory
# into /src folder (see .dockerignore file for stuff we don't need on the server)
COPY . /src

# workdir to /src
WORKDIR /src

# we need gcc and g++ compilers:
RUN apt-get update \
    && apt-get -y install gcc \
    && apt-get -y install g++

# cv2 needs some special dependencies (some libsm stuff),
# which aren't in this base image, so we install it:
RUN apt-get install ffmpeg libsm6 libxext6  -y

# upgrade pip and then install requirements.txt
RUN pip install --upgrade pip \
    && pip install -r req_docker/requirements.txt


# re-install of pycocotools necesarry because of some weird changes in the numpy C API,
# when we install a newer version of numpy, that is needed by imageai.
RUN pip install -r req_docker/requirements_imageai.txt

# fix permission error: (heroku runs this as a non root user)
# files in folders, in which we save incomming images and the images with bboxes
RUN chmod -R a+rwx static/uploads \
    && chmod -R a+rwx test_images_with_boxes

# to test image locally, run as non-root user:
RUN useradd -m nonrootuser
USER nonrootuser

# this will be what we run with python (the actual flask app):
# entrypoint or CMD defines what we do after building and starting the above defined container
CMD ["python", "Flask.py"]
